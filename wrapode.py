"""
Decorators for ODE rhs. Sadly, do not work with the numba-accelerated solvers
from nbkode.
"""

def complex(ż_fun):
    from numpy import concatenate
    def ẏ_fun(t, y):
        N = len(y)
        ż = ż_fun(t, y[:N//2] + 1j*y[N//2:])
        return concatenate((ż.real, ż.imag))

    return ẏ_fun


def twovar(ȧḃ_fun):
    def ẏ_fun(t, y):
        return ȧḃ_fun(t, y[0], y[1])

    return ẏ_fun
