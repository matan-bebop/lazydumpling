# vim: set ts=4:sw=4:expandtab


def const_self(fun):
    """ Decorate a method `fun` to allow it accessing some of the class
    properties read-only.

    `fun` has some arguments that have the same names as the class properties---
    so the decorator calls your function passing the respective properties.

    For example, consider
    >>> class QuadraticEquation:
    ...     def __init__(self, a, b, c):
    ...         self.a = a
    ...         self.b = b
    ...         self.c = c
    ...
    ...     @const_self
    ...     def roots(a, b, c, dummy, lazy = False):
    ...         from cmath import sqrt
    ...
    ...         print("Dummy is", dummy)
    ...         if lazy: return # A lazy calculator does not calculate
    ...         return((-b - sqrt(b**2 - 4*a*c)) / (2*a),
    ...                (-b + sqrt(b**2 - 4*a*c)) / (2*a))
    ...
    ...     @const_self
    ...     def __str__(a, b, c):
    ...         def pretty_num(n):
    ...             return "" if n == 1 else str(n)
    ...            
    ...         fmt_ax2 = ("-" if a<0 else "") + pretty_num(abs(a)) + " x^2"
    ...         fmt_bx = (" - " if b<0 else " + ") + pretty_num(abs(b)) + " x"
    ...         fmt_c = (" - " if c<0 else " + ") + pretty_num(abs(c))
    ...
    ...         return fmt_ax2 + fmt_bx + fmt_c
    ...
    
    That yields:
    >>> e = QuadraticEquation(1,3,-4)
    >>> print("The ", e, " equation roots are:", e.roots("yummy"))
    Dummy is yummy
    The   x^2 + 3 x - 4  equation roots are: ((-4+0j), (1+0j))
    """
    fun_args = fun.__code__.co_varnames[:fun.__code__.co_argcount]

    def fun_with_self(*args, **kwargs):
        self = args[0]
        other_args = list(args[1:])

        used_attributes = [arg for arg in fun_args if hasattr(self, arg)]
        self_args = [getattr(self, attr) for attr in used_attributes]

        return fun(*(self_args + other_args), **kwargs)
    
    return fun_with_self
