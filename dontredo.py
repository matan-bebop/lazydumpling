# vim: set ts=4:sw=4:expandtab

def dont_redo_with_same(dict, parameter_names):
    """ Create a decorator that only calls a function when some of
    `parameter_names` variables change in the `dict`

    Let's define a very important global parameter:
    >>> buba = "baba"

    and a function that does a heavy calculation depending on it:
    >>> def kuka(): print(buba)
    ... 

    Naturally, each time you call kuka() the calculation repeats:
    >>> kuka()
    baba
    >>> kuka()
    baba

    Let's avoid that:    
    >>> remember_buba = dont_redo_with_same(globals(), ["buba"])
    >>> hytrjuka = remember_buba(kuka);
    >>> hytrjuka()
    baba
    >>> hytrjuka()
    >>> buba = "dido"
    >>> hytrjuka()
    dido
    >>> hytrjuka()
    """
    def init_parameters_check(fun):
        fun.calculated_parameters = {}
        fun.last_result = None

    def current_parameters():
        return [dict[name] for name in parameter_names]

    def calculated(fun):
        fun.calculated_parameters = current_parameters()

    def is_calculated(fun):
        return fun.calculated_parameters == current_parameters()

    def decorator(fun):
        """ Decorate `fun` to be called only when the parameters change
        """
        init_parameters_check(fun)
        def wrapper(): 
            if not is_calculated(fun):
                fun.last_result = fun()
            calculated(fun)
            return fun.last_result 

        return wrapper
    
    return decorator
