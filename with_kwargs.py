# vim: set ts=4:sw=4:expandtab

def with_kwargs(fun, **add_kwargs):
	""" Decorate fun to be always called with add_kwargs """
    def newfun(*args, **kwargs):
        all_kwargs = {**kwargs, **add_kwargs}
        return fun(*args, **all_kwargs)
    return newfun
