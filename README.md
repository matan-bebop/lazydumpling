`lazydumpling` is a collection of tools to simplify the life of a simple-minded
coder presumably writing very straightforward scripts for research purposes.
Usually the research purposes are not so straighforward, but that is not a must
for using `lazydumpling`.

To fully adhere the `lazydumpling` philosophy, please try out this delicious
dish. See https://klopotenko.com/lenuvi-vareniki-z-jogurtovym-sousom/. 

For the impatient
-----------------

You can use:

`constself` if have a model with many parameters in a class, and you are tired
of reading and writing your precious formulas with so many selfs! Do not use it
if you don't like avoiding self.

`dontredo` if you just defined all your parameters as global variables, you 
have a function that depends on them, and you want to cache its last call

`wrapode` if your solver (or whatever) only accepts an array of parameters
in the function you provide to it, and you want to declare this function
nicely as a function of a complex number, or you want it to be a function of
two real arguments explicitely

`with_kwargs` if you call a function with a definite set of kwargs many times
